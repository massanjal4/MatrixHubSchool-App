package tz.co.matrixhub.matrixhubschool;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import me.itangqi.waveloadingview.WaveLoadingView;

public class MainActivity extends AppCompatActivity implements ServerManager.ServerResponseHandler {

    final private int GET_SYSTEM_INFO_REQUEST = 1000;
    final private int LOGIN_REQUEST = 1001;
    private ServerManager serverManager;
    private ProgressDialog progressDialog;
    WaveLoadingView waveLoadingView;

    private AnimationDrawable animationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MatrixHubSharedPrefs", 0);
        if (preferences.getString("USERID", null) != null) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }

        RelativeLayout relativeLayout =  findViewById(R.id.relative_main);
        animationDrawable = (AnimationDrawable) relativeLayout.getBackground();

        // setting enter fade animation duration to 5 seconds
        animationDrawable.setEnterFadeDuration(6000);

        // setting exit fade animation duration to 2 seconds
        animationDrawable.setExitFadeDuration(7000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            // start the animation
            animationDrawable.start();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            // stop the animation
            animationDrawable.stop();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initValues();
    }

    // initialize values and settings
    public void initValues() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        TextView copyright = (TextView) findViewById(R.id.copyright_textView);
        copyright.setText("Copyright MatrixHub "+year);
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int logoSize = outMetrics.heightPixels/6;
        ImageView logoView = (ImageView)findViewById(R.id.logo_imageView);
        logoView.getLayoutParams().height = logoSize;
        logoView.getLayoutParams().width = logoSize;
        EditText email = (EditText)findViewById(R.id.email_editText);
        email.setText("");
        EditText password = (EditText)findViewById(R.id.password_editText);
        password.setText("");
        serverManager = new ServerManager(this);
        serverManager.getSystemInfo(GET_SYSTEM_INFO_REQUEST);
        waveLoadingView = new WaveLoadingView(this);
        progressDialog = new ProgressDialog(this);
    }

    // login action
    public void loginAction(View view) {
        EditText emailText = (EditText)findViewById(R.id.email_editText);
        String email = emailText.getText().toString().trim();
        if (email.length() == 0) {
            emailText.setError("Enter Email Address");
            return;
        }
        EditText passwordText = (EditText)findViewById(R.id.password_editText);
        String password = passwordText.getText().toString().trim();
        if (password.length() == 0) {
            passwordText.setError("Enter Password");
            return;
        }
        waveLoadingView = (WaveLoadingView)findViewById(R.id.waveLoadingView);
        waveLoadingView.setProgressValue(55);
        waveLoadingView.setVisibility(View.VISIBLE);
        waveLoadingView.setCenterTitle("Loading...");
        waveLoadingView.startAnimation();
        progressDialog.setMessage("TItle");
        progressDialog.setTitle("Signing In...");
        progressDialog.setMessage("Please wait...!!!");
        progressDialog.setCanceledOnTouchOutside(false);
        //progressDialog.show();
        serverManager.login(email, password, LOGIN_REQUEST);
    }

    // forgot password button action
    public void forgotPasswordAction(View view) {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    // server response methods
    @Override
    public void requestFinished(String response, int requestTag) {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        try {
            JSONObject responseJson = new JSONObject(response);
            //Log.d("test", responseJson.toString());
            if (requestTag == GET_SYSTEM_INFO_REQUEST) {
                TextView schoolName = (TextView)findViewById(R.id.app_name_textView);
                schoolName.setText(responseJson.optString("system_name"));
            }
            if (requestTag == LOGIN_REQUEST) {
                if (responseJson.optString("status").equals("success")) {
                    String loginType = responseJson.optString("login_type");
                    String userId = responseJson.optString("login_user_id");
                    String userName = responseJson.optString("name");
                    String authKey = responseJson.optString("authentication_key");
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("MatrixHubSharedPrefs", 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("LOGINTYPE", loginType);
                    editor.putString("USERID", userId);
                    editor.putString("USERNAME", userName);
                    editor.putString("AUTHKEY", authKey);
                    if (loginType.equals("student")) {
                        editor.putString("CLASSID", responseJson.optString("class_id"));
                    }
                    editor.apply();
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                }
                else
                    waveLoadingView.setVisibility(View.INVISIBLE);
                    waveLoadingView.pauseAnimation();
                    showAlert("Login Failed, check your credentials.");
            }
        }
        catch (JSONException e) {
            //do nothing
        }
    }

    @Override
    public void requestFailed(String errorMessage, int requestTag) {
        //Log.d("TAG", "Request failed");
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        if (requestTag == LOGIN_REQUEST)
            showAlert("Login failed");
    }

    @Override
    public void imageDownloaded(Bitmap image, int requestTag) {

    }

    // shows an alert
    public void showAlert(String message) {
        AlertDialog alert = (new AlertDialog.Builder(this)).setMessage(message).setPositiveButton("Ok", null).create();
        alert.show();
    }
}
