package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class ExamInfo {
    private String examId;
    private String examName;
    public ExamInfo(String examId, String examName) {
        this.examId = examId;
        this.examName = examName;
    }
    public String getExamId() {
        return examId;
    }
    public String getExamName() {
        return examName;
    }
}
