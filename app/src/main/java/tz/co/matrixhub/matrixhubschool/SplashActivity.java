package tz.co.matrixhub.matrixhubschool;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.Calendar;

import gr.net.maroulis.library.EasySplashScreen;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int year = Calendar.getInstance().get(Calendar.YEAR);

        EasySplashScreen config = new EasySplashScreen(SplashActivity.this)
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(7000)
                .withAfterLogoText(getString(R.string.app_name))
                .withBackgroundColor(Color.parseColor("#122739"))
                .withLogo(R.drawable.logo)
                .withFooterText("Copyright MatrixHub "+year);

        config.getFooterTextView().setTextColor(android.graphics.Color.rgb(255,255,255));
        config.getAfterLogoTextView().setTextColor(android.graphics.Color.rgb(255,255,255));

        View view = config.create();

        setContentView(view);
    }

}
