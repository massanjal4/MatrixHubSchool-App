package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class StudentMarkInfo {
    private String subjectName;
    private String mark;
    private String grade;
    public StudentMarkInfo(String subjectName, String mark, String grade) {
        this.subjectName = subjectName;
        this.mark = mark;
        this.grade = grade;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public String getMark() {
        return mark;
    }

    public String getGrade() {
        return grade;
    }
}
