package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class AttendanceInfo {
    private String studentRoll;
    private String studentName;
    private String status;
    public AttendanceInfo(String studentRoll, String studentName, String status) {
        this.studentRoll = studentRoll;
        this.studentName = studentName;
        this.status = status;
    }

    public String getStudentRoll() {
        return studentRoll;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getStatus() {
        return status;
    }
}
