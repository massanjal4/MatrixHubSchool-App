package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class SubjectInfo {
    private String subjectId;
    private String subjectName;
    private String teacherName;
    public SubjectInfo(String subjectId, String subjectName, String teacherName) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.teacherName = teacherName;
    }

    public String getSubjectId() {
        return subjectId;
    }
    public String getSubjectName() {
        return subjectName;
    }
    public String getTeacherName() {
        return teacherName;
    }
}
