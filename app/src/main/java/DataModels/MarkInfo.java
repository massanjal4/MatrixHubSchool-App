package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class MarkInfo {
    private String studentRoll;
    private String studentName;
    private String mark;
    public MarkInfo(String studentRoll, String studentName, String mark) {
        this.studentRoll = studentRoll;
        this.studentName = studentName;
        this.mark = mark;
    }

    public String getStudentRoll() {
        return studentRoll;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getMark() {
        return mark;
    }
}
