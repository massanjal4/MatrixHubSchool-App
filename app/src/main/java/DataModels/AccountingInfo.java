package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class AccountingInfo {
    private String title;
    private String amount;
    private String timestamp;
    private String dueAmount;
    public AccountingInfo(String title, String amount, String dueAmount, String timestamp) {
        this.title = title;
        this.amount =amount;
        this.dueAmount = dueAmount;
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public String getAmount() {
        return amount;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
