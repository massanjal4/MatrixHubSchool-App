package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class ClassInfo {
    private String classId;
    private String className;
    public ClassInfo(String classId, String className) {
        this.classId = classId;
        this.className = className;
    }
    public String getClassId() {
        return this.classId;
    }
    public String getClassName() {
        return this.className;
    }
}
