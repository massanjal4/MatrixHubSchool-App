package DataModels;

/**
 * Created by lawrance on 17/01/2019.
 * © 2019
 */
public class SectionInfo {
    private String sectionID;
    private String sectionName;
    private String classID;

    public SectionInfo(String sectionID, String sectionName, String classID) {
        this.sectionID = sectionID;
        this.sectionName = sectionName;
        this.classID = classID;
    }

    public String getSectionID() {
        return sectionID;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getClassID() {
        return classID;
    }
}
